const fs = require("fs");

// fs.readFile('./starter.txt',(err,data)=>{
//     if(err) throw err;
//     console.log(data); // buffer data
//     console.log(data.toString());
// })
/****************autre methode */
const path = require('path');
//path.join(__dirname,nom_du_dossier,'starter.txt')
fs.readFile(path.join(__dirname,'starter.txt'), "utf8", (err, data) => {
  if (err) throw err;
  console.log(data);
});

console.log('Hello...'); // afficher le 1 ér sur le terminal au meme temps node termine lu le fichier
//********** */ creé file 
// fs.writeFile(path.join(__dirname,'reply.txt'),'Nice to meet you.' ,(err) => {
//     if (err) throw err;
//     console.log("Write complete");
//   });
// joue le role aussi de crée file
fs.appendFile(path.join(__dirname,'test.txt'),'Testing text.' ,(err) => {
    if (err) throw err;
    console.log("Append  complete");
  });
  fs.writeFile(path.join(__dirname,'reply.txt'),'Nice to meet you.' ,(err) => {
    if (err) throw err;
    console.log("Write complete");
    fs.appendFile(path.join(__dirname,'reply.txt'),'\n\nYes it is.' ,(err) => {
        if (err) throw err;
        console.log("Append  complete");

        fs.rename(path.join(__dirname,'reply.txt'),path.join(__dirname,'newReply.txt') ,(err) => {
            if (err) throw err;
            console.log("Rename  complete");
          });
      });
  });


// exit on uncaught errors
process.on("uncaughtException", (err) => {
  console.log(`There was an uncaught error:${err}`);
  process.exit(1);
});
