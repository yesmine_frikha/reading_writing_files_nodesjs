const fsPromises = require("fs").promises;
const path = require("path");
const fileOps = async () => {
  try {
    const data = await fsPromises.readFile(
      path.join(__dirname, "starter.txt"),
      "utf8"
    );
    console.log(data);
    //delete
    await fsPromises.unlink(path.join(__dirname, "starter.txt"));
    await fsPromises.writeFile(path.join(__dirname, "promiseWrite.txt"),data);
    await fsPromises.appendFile(path.join(__dirname, "promiseWrite.txt"),'\n\n Nice to meet you.');
    await fsPromises.rename(path.join(__dirname, "promiseWrite.txt"),path.join(__dirname, "promiseComplete.txt"));  
    const newdata = await fsPromises.readFile(
        path.join(__dirname, "promiseComplete.txt"),
        "utf8"
      );
    console.log(newdata);
} catch (err) {
    console.error(err);
  }
};
fileOps();
