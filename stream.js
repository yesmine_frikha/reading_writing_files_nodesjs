const fs = require('fs');

// Création du flux de lecture
const rs = fs.createReadStream('./lorem.txt', { encoding: 'utf8' });

// Création du flux d'écriture
const ws = fs.createWriteStream('./new-lorem.txt');

// rs.on('data', (dataChunk) => {
//     ws.write(dataChunk);
// });

rs.pipe(ws);